import './App.css';
import Imagelist from "./components/Imagelist"
import Header from "./components/Header"
import UploadModal from './components/UploadModal';
import { useState} from 'react'





function App() {
  const [isModalOn, setModalOn] = useState(false)

function getUploadModalStatus(modalStatus){
  setModalOn(modalStatus)
}
  return (
    <>
      <Header getUploadModalStatus={getUploadModalStatus}/>
      <div className="container">
         <Imagelist/>
      </div>
        {
        isModalOn 
        ? <UploadModal isModalOn={isModalOn} setModalOn={setModalOn} /> : 
          null
        }
    </>
  );
}

export default App;

