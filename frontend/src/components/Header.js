import React  from 'react'
import {AiOutlineSearch} from "react-icons/ai"

import './header.css'
import logo from "../assets/images/logo.jpeg"


export default function Header({getUploadModalStatus}) {
  
// const [isModalOpen, showModal] = useState(false)
  return (
    <div className='header'>
        <div className='logo'>
            <img src={logo} alt='logo'/>
        </div>
        <div className='search'>
            <div className='search-icon'>
                 <AiOutlineSearch style={{marginTop: 10}} size={25}/>
            </div>
            <input type="text" placeholder='search for image ...'/>
        </div>
        <button className='upload' 
          onClick={()=>{
            getUploadModalStatus(true)
          }}
          >Upload Image
        </button>
    </div>
  )
}
 
