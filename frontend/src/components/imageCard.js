import React from 'react'
import {HiClipboardCopy} from 'react-icons/hi'
import './imageCard.css'
import { useState} from 'react'



export default function ImageCard({name, image, url}) {

  const [clicked, setClicked] = useState(false)
  const copy = async () => {
    await navigator.clipboard.writeText(url);
    setClicked(!clicked)
  }
  setTimeout(()=>{setClicked(false)},500)
  

  return (
    <>
      <section className='image-card'>
          <img src={image} alt={name ||'uploaded file images'}></img>
        <div className='image-name'>
          {name}
        </div>
        <div className='url-container'>
          <div className='image-url'>
            {/* {url.slice(0,20)+'...'+url.slice(-6)} */}
          </div>
          <div className='copy-icon' onClick={copy}>
          <HiClipboardCopy size={28}/>
          <div className={clicked?"alert":"hide"}>url copied</div>
          </div>
        </div>
      </section>
    </>
  )
}
