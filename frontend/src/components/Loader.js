import React from 'react'
import uploading from "../assets/images/loader.gif"
import loading from "../assets/images/download.gif"

export default function Loader({loadingStatus}) {
    return (
        <div>
            {
            loadingStatus="UPLOAD"?
            <img src={uploading} alt='uploading'/>
            :<img src={loading} alt='downloading'/>
            }
        </div>
      )
}

