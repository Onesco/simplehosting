import React, {useEffect, useState} from 'react'
import axios from '../API/axios'
import images from '../API/dummyDaata'
import ImageCard from './imageCard'
import './ImageList.css'




export default function Imagelist() {
const [data, setData] = useState(images)

let imageList = [...images, ...data]
function renderImages(){
  return imageList.map((image, index)=>{
    return(
      <div>
      <ImageCard key={index}  name={image?.name} image={image?.imageUrl} url={image?.imageUrl}/>
    </div>
    )
  })
}

useEffect(() => {
  console.log('useEffect')
  let fetchData = async ()=>{
    let {data} = await axios.get('/images')
    console.log(data)
    setData(data)
  }
  fetchData()
}, [])

  return (
    <div className='image-list'>
      {renderImages()}
    </div>
  )
}
