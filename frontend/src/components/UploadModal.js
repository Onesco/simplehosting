import React, { useRef, useState } from "react";
import ReactDom from "react-dom";
import axios from "../API/axios";

import Loader from "./Loader";

import './Upload.css'


export default function UploadModal({ setModalOn }) {

    const [inputValue,  setinputValue ] = useState(()=>'')
    const [image,  setImage ] = useState(()=>'')
    const [uploadStatus, setUploadStatus] = useState(()=>false)
    
    // close the modal when clicking outside the modal.
    const modalRef = useRef();

    const closeModal = (e) => {
      if (e.target === modalRef.current) {
        setModalOn(false);
      }
    };

    const onFileUpload= (e)=>{
       let file = e.target.files[0]
       
       let fileReader = new FileReader()
       fileReader.readAsDataURL(file)
       fileReader.onload=(e)=>{
        setImage(e.target.result)
       }
    }

    const url ='/upload'
    const formData = {
        name:inputValue,
        imageUrl:image
    }

    const onSubmit = async(e)=>{
      console.log('hello onsomit')
        e.preventDefault()
        setUploadStatus(true)
        let response = await axios.post(url, formData)  
        if(response.status===200){
          setUploadStatus(false)
        }   
    }

    //render the modal JSX in the portal div.
    return ReactDom.createPortal(
      <div className="modal-container" ref={modalRef} onClick={closeModal}>
        <div className="modal">
          <button className="btn close" onClick={() => setModalOn(false)}>X</button>
          <div className="modal-input">
            <form>
                {
                  uploadStatus
                  ?
                  <Loader loadingStatus='UPLOAD'/>
                  :
                  <>
                    <textarea 
                      placeholder="Description: not more than 100 characters" 
                      rows={3} cols={40} maxLength={100} 
                      onChange={(e)=>{setinputValue(e.target.value)}}
                    />
                    <input onChange={(e)=>{onFileUpload(e)}} type='file' name="imageUrl"/>
                    <button onClick={(e)=>{onSubmit(e)}} className="btn submit">submit</button>
                  </>
                }
            </form>
          </div>
        </div>
        <div>
        </div>
      </div>,
      document.getElementById("portal")
    );
  };



 
