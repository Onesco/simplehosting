import axios from 'axios'

export default axios.create(
    {
        baseURL:'https://image-host-in.herokuapp.com/'
    }
)