const Joi = require("joi");

const {imageHosting} = require("./joiSchemas")


let JoiValidator = (input, schema) => {
    const { error, value } = schema.validate(input);
    if (error) {
      let message = error.details.map((el) => el.message).join("\n");
      return {
        message,
        type: "ValidationError",
        code: 422,
      };
    }
    return value;  
  };



  module.exports = {
    imageHostingValidator : (payload) => JoiValidator(payload, imageHosting)
  }
  