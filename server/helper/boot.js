const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')
const morgan = require('morgan');
const routerMounter = require('../routers');


const boot = (app) => {
    app.use(cors())
    app.use(express.urlencoded({
      extended: false,
    }));
    app.use(morgan('dev', {
      stream: {
        write: (msg) => {
          console.log(msg);
        },
      },
    }));
    app.use(bodyParser.urlencoded({ 
      extended: true 
      }));
    //  app.use(auth);
    
    if (app.get('env') == 'production') {
      app.use(morgan('combined', {
        stream: reqLog,
      }));
    };
    app.use(express.json());
    app.use(bodyParser.json());
    routerMounter(app);    
  }

  module.exports = {
    boot
  }