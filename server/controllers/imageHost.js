const ImageData = require("../db/data-access/imageHost");
const BaseController = require ("./base")



module.exports = class Image extends BaseController {

    static async test(req, res, next){
        return this.response(res, {
            message:"Hello world"
        })
    }

    static async uploadImage(req, res, next){

        const {name, imageUrl} = req.body

        if(!name){
            return this.response(res, {
                message: "images must not be uploaded without name"
            })
        }
        if(!imageUrl){
            return this.response(res, {
                message: "images must must have a valid url"
            })
        }
        const uploadedImage = await ImageData.uploadImage({
            name,
            imageUrl
        })
        this.response(res, uploadedImage)
    }

    static async updateImage(req, res, next){

        const {name, imageUrl} = req.body

        if(!name){
            return this.response(res, {
                message: "images must not be uploaded without name"
            })
        }
        const uploadedImage = await ImageData.uploadImage({
            name,
            imageUrl
        })
        return this.response(res, uploadedImage)
    }

    static async getAllImages(req, res, next){

        const allIImages = await ImageData.getImageBy()
        return this.response(res,allIImages)
    }
}
