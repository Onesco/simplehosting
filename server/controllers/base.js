module.exports = class BaseController{
  static response(res, obj) {
    return res.json(obj);
  }
  static toCapitalLetter(name) {
    let upperCaseName = [];
    let nameArray = name.split(" ");
    for (let name of nameArray) {
      name === ""
        ? ""
        : (name = name[0].toUpperCase() + name.slice(1).toLowerCase());
      if (name !== "") upperCaseName.push(name);
    }
    return upperCaseName.join(" ");
  }
}


