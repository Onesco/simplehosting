const { Router } = require("express");
const router = Router();
const {imageHostingValidator} = require("../validator")
const Controllers = require("../controllers/imageHost")

function validate(data, validator, next, res) {
    const err = validator(data);
    if (err && (err.type || err.code || err.message)) {
      const error = new Error(err.message);
      error.type = err.type;
      error.code = err.code;
      res.statusCode = err.code
      res.json(err);
    } else {
      next();
    }
  }
  
  // validate request body
  const validateBody = function (reqValidator) {
    return function (req, res, next) {
      const input = req.body;
      return validate(input, reqValidator, next, res);
    };
  }
  
  // validate request params
  const validateParams = function (reqValidator) {
    return function (req, res, next) {
      const input = req.params;
      return validate(input, reqValidator, next);
    };
  }
  



router.get('/',(req, res, next)=> Controllers.test(req, res))
router.get('/images',(req, res, next)=> Controllers.getAllImages(req, res, next))

router.post('/upload', validateBody(imageHostingValidator),(req, res, next)=> Controllers.uploadImage(req, res, next))

module.exports = router