const hostingImagesRoute = require('./hostimages')

module.exports = function (app) {
  app.use("/", hostingImagesRoute);
}
