const http = require('http');
const express = require('express');
const mongoose =  require('mongoose')
const {boot} = require('./helper/boot');
const {dbConnection} = require('./db/connect')

// load env once
require('./helper/load-env')();

const app = express();

app.set('title', process.env.APP_NAME || 'APP_NAME');
app.set('env', process.env.APP_ENV || 'APP_ENV');

// make the mongoDb connection
dbConnection(mongoose)

// boot app  and setup app middlewares and routes
boot(app);

const server = http.createServer(app);

server.listen(process.env.PORT||4002, (err) => {
  if (err) {
    console.log(err);
    return 1;
  }
  console.log('HTTP running on port ' + process.env.PORT);
});
