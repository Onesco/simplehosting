const dbConnection = (db)=>{
    const username = process.env.MONGO_USER;
    const password = process.env.MONGO_PW;

    const url = `mongodb+srv://${username}:${password}@cluster0.lg4fq.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`;

    const connectionParams={
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true 
    }
    db.connect(url,connectionParams)
        .then( () => {
            console.log('Connected to the database ')
        })
        .catch( (err) => {
            console.error(`Error connecting to the database. n${err}`);
        })
}

module.exports = {
    dbConnection
}