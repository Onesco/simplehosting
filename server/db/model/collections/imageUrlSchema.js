const mongoose =  require('mongoose')

let Schema = mongoose.Schema;
let imageUrlSchema = new Schema({
    imageUrl:{type: String,  required: true}, 
    name: {type: String, required: true}
});

module.exports = mongoose.model("ImageUrl", imageUrlSchema)