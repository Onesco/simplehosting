
const {ImageModel} = require('../model/index')

module.exports = class ImageData {
  static async getOneImageBy(fields = {}, sort = { _id: 1 }) {
    return await ImageModel.findOne(fields).sort(sort);
  }

  static async getImageBy(fields = {}, sort = { _id: 1 }, pageNum = 0, pageLimit = 10) {
    return await ImageModel.find(fields).sort(sort);
  }

  static async uploadImage(fields = {}) {
    const newImage = new ImageModel(fields);
    return await newImage.save();
  }

  static async updateImage(fields = {}, updates = {}) {
    return await ImageModel.findOneAndUpdate(fields, updates, {new:true});
  }

  static async deleteImage(fields = {}) {
    return await ImageModel.findOneAndRemove(fields = {});
  }
}